The rails app is located in the shipstr directory. Start the app on port 3000 with the following commands...

```
cd shipstr
bundle install
rake db:drop
rake db:create
rake db:migrate
rake db:seed
rails server
```

To start the Vue app:

```
cd vue-spa
npm i
npm run serve
```

Navigate to http://localhost:8080/ to view shipping data.
